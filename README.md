 ##Aspire Test##

# 1. Set up
- Update browser want to test to versions latest before run test
- Download and install IntelliJ of JetBrains (Community): 
https://www.jetbrains.com/idea/download/#section=windows
- Open project with IntelliJ
- Setup JDK 1.8
- Setup Maven 3.6.3

# 2. How to run Test
- Click run test file class AspireTestRegister
- View report file ReportResults.html
- View img report file folder ReportIMG



