set chrome-driver=-Dwebdriver.chrome.driver="../driver/chromedriver.exe"
set firefox-driver=-Dwebdriver.gecko.driver="../driver/geckodriver.exe"

set chrome-browser=-browser "browserName=chrome, version=ANY, platform=WINDOWS, maxInstances=5"
set firefox-browser=-browser "browserName=firefox, version=ANY, platform=WINDOWS, maxInstances=5"

set browsers-config=%chrome-browser% %firefox-browser% 

set driver-path=%chrome-driver% %firefox-driver% %ie-driver%
set hub-url=http://localhost:4444/grid/register

java %driver-path% -jar selenium-server-standalone-3.141.59.jar -role node -hub %hub-url% -port 5566 %browsers-config%