package com.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

public class BusinessStandard {
    public WebDriver driver;

    public BusinessStandard(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Xpath
    @FindBy(how = How.XPATH, using = "(//div//button[@type='button'])[2]")
    private WebElement btnGetStart;
    @FindBy(how = How.XPATH, using = "//div//button[@type='submit']")
    private WebElement btnGetStart2;
    @FindBy(how = How.XPATH, using = "//input[@placeholder='Set your date of birth']")
    private WebElement txtCalender;
    @FindBy(how = How.XPATH, using = "(//div//span[@class='block'])[2]")
    private WebElement txtCurrentMonth;
    @FindBy(how = How.XPATH, using = "(//div//span[@class='block'])[3]")
    private WebElement txtCurrentYear;
    @FindBy(how = How.XPATH, using = "(//div[@class='col-auto'])[1]")
    private WebElement txtCurrentYearBack;
    @FindBy(how = How.XPATH, using = "(//div//span[@class='block'])[17]")
    private WebElement txtCurrentYear1995;
    @FindBy(how = How.XPATH, using = "(//div//span[@class='block'])[7]")
    private WebElement txtCurrentMonth06;
    @FindBy(how = How.XPATH, using = "(//div//span[@class='block'])[20]")
    private WebElement txtCurrentDay17;
    @FindBy(how = How.XPATH, using = "(//div//i[@role='presentation'])[2]")
    private WebElement btnNationality;
    @FindBy(how = How.XPATH, using = "(//div//i[@role='presentation'])[3]")
    private WebElement btnGender;
    @FindBy(how = How.XPATH, using = "(//div//i[@role='presentation'])[4]")
    private WebElement btnInterested;

    public void fillonBusinessStandard() throws Exception {
        btnGetStart.click();
        Thread.sleep(1000);
        btnGetStart2.click();
        Thread.sleep(1000);
        txtCalender.click();
        Thread.sleep(5000);
        txtCurrentYear.click();
        Thread.sleep(2000);
        txtCurrentYearBack.click();
        Thread.sleep(2000);
        txtCurrentYearBack.click();
        Thread.sleep(2000);
        txtCurrentYear1995.click();
        Thread.sleep(2000);
        txtCurrentMonth.click();
        Thread.sleep(2000);
        txtCurrentMonth06.click();
        Thread.sleep(2000);
        txtCurrentDay17.click();
        Thread.sleep(2000);

        //Roll menu tag page up down
        try {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        } catch (Exception e) {
            System.out.println(e);
        }

        btnNationality.click();
        Thread.sleep(2000);
        // Random about nationality
        List<WebElement> nationality = driver.findElements(By.xpath("(//div//div[@class='q-item__label'])"));
        int count = nationality.size();
        System.out.println("Number of links are:" + count);
        Random r = new Random();
        int linkNo = r.nextInt(count);
        WebElement nationality2 = nationality.get(linkNo);
        System.out.println(nationality2.getText());
        Thread.sleep(2000);
        nationality2.click();

        //Roll menu tag page up down
        try {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        } catch (Exception e) {
            System.out.println(e);
        }

        btnInterested.click();
        Thread.sleep(2000);
        // Random about Interested
        List<WebElement> Interested = driver.findElements(By.xpath("(//div//div[@class='q-item__label'])"));
        int counttt = Interested.size();
        System.out.println("Number of links are:" + counttt);
        Random r3 = new Random();
        int linkNoo0 = r3.nextInt(counttt);
        WebElement Interested2 = Interested.get(linkNoo0);
        System.out.println(Interested2.getText());
        Thread.sleep(2000);
        Interested2.click();

        btnGender.click();
        Thread.sleep(2000);
        // Random about gender
        List<WebElement> gender = driver.findElements(By.xpath("(//div//div[@class='q-item__label'])"));
        int countt = gender.size();
        System.out.println("Number of links are:" + countt);
        Random r2 = new Random();
        int linkNoo = r2.nextInt(countt);
        WebElement gender2 = gender.get(linkNoo);
        System.out.println(gender2.getText());
        Thread.sleep(2000);
        gender2.click();

    }


}
