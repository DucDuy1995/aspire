package com.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

public class BusinessDetails {
    public WebDriver driver;

    public BusinessDetails(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Xpath
    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement btnSubmit;
    @FindBy(how = How.XPATH, using = "//div//input[@data-cy='register-business-name']")
    private WebElement txtBusinessName;
    @FindBy(how = How.XPATH, using = "(//div//i[@role='presentation'])[1]")
    private WebElement btnRegistrationType;
    @FindBy(how = How.XPATH, using = "//div//input[@data-cy='register-business-registration-numer']")
    private WebElement txtBusinessNumber;
    @FindBy(how = How.XPATH, using = "(//div//i[@role='presentation'])[2]")
    private WebElement btnIndustry;
    @FindBy(how = How.XPATH, using = "(//div//i[@role='presentation'])[3]")
    private WebElement btnSubIndustry;

    public void fillonBusinessDetail() throws Exception {
        btnSubmit.click();
        Thread.sleep(5000);
        // Random about business
        List<WebElement> business = driver.findElements(By.xpath("//div[@class='auth-form__card']//div[@role='checkbox']"));
        int countt = business.size();
        System.out.println("Number of links are:" + countt);
        Random r = new Random();
        int linkNoo = r.nextInt(countt);
        WebElement business2 = business.get(linkNoo);
        System.out.println(business2.getText());
        Thread.sleep(2000);
        business2.click();

        Random r2 = new Random();
        int numberbusiness = r2.nextInt(999);
        txtBusinessName.clear();
        txtBusinessName.sendKeys("business" + numberbusiness);
        Thread.sleep(2000);

        btnRegistrationType.click();
        Thread.sleep(1000);
        // Random about RegistrationType
        List<WebElement> RegistrationType = driver.findElements(By.xpath("(//div//div[@class='q-item__label'])"));
        int count = RegistrationType.size();
        System.out.println("Number of links are:" + count);
        Random r3 = new Random();
        int linkN = r3.nextInt(count);
        WebElement RegistrationType2 = RegistrationType.get(linkN);
        System.out.println(RegistrationType2.getText());
        Thread.sleep(2000);
        RegistrationType2.click();

        Random r4 = new Random();
        int numberUEN = r4.nextInt(99999989) + 10;
        txtBusinessNumber.clear();
        txtBusinessNumber.sendKeys(numberUEN + "a");

        //Roll menu tag page up down
        try {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        } catch (Exception e) {
            System.out.println(e);
        }

        btnIndustry.click();
        Thread.sleep(1000);
        // Random about Industry
        List<WebElement> Industry = driver.findElements(By.xpath("(//div//div[@class='q-item__label'])"));
        int counI = Industry.size();
        System.out.println("Number of links are:" + counI);
        Random r5 = new Random();
        int linkNN = r5.nextInt(counI);
        WebElement Industry2 = Industry.get(linkNN);
        System.out.println(Industry2.getText());
        Thread.sleep(2000);
        Industry2.click();

        btnSubIndustry.click();
        Thread.sleep(1000);
        // Random about SubIndustry
        List<WebElement> SubIndustry = driver.findElements(By.xpath("(//div//div[@class='q-item__label'])"));
        int counS = SubIndustry.size();
        System.out.println("Number of links are:" + counS);
        Random r6 = new Random();
        int linkSub = r6.nextInt(counS);
        WebElement SubIndustry2 = SubIndustry.get(linkSub);
        System.out.println(SubIndustry2.getText());
        Thread.sleep(2000);
        SubIndustry2.click();

        btnSubmit.click();
        Thread.sleep(30000);


    }
}
