package com.qa.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class OTPMail {
    public WebDriver driver;

    public OTPMail(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    //Xpath
    @FindBy(how = How.XPATH, using = "//div//button[@type='submit']")
    private WebElement btnContinue;
    @FindBy(how = How.XPATH, using = "//div[@class='digit-input']//input[@autocomplete='one-time-code']")
    private WebElement txtOTPMail;

    public void sendOTPMail() throws Exception{
        btnContinue.click();
        Thread.sleep(10000);
        txtOTPMail.sendKeys("1234");
        Thread.sleep(5000);
    }
}
