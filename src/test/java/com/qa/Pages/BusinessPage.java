package com.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class BusinessPage {
    public WebDriver driver;

    public BusinessPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Xpath
    @FindBy(how = How.XPATH, using = "//div//button[@type='submit']")
    private WebElement btnContinueRegisterSuccess;
    @FindBy(how = How.XPATH, using = "(//div//button[@type='button'])[1]")
    private WebElement btnContinue;
    @FindBy(how = How.XPATH, using = "(//div//button[@type='button'])[1]")
    private WebElement btnGetStart;

    public void fillonBusiness() throws Exception {
        btnContinueRegisterSuccess.click();
        Thread.sleep(2000);
        btnContinue.click();
        Thread.sleep(5000);
        btnGetStart.click();
        Thread.sleep(2000);
    }

    //Xpath
    @FindBy(how = How.XPATH, using = "//button[@class='btn btn-danger']")
    private WebElement btnLogin;

    public void loginBusiness() throws Exception {
        btnLogin.click();
        Thread.sleep(5000);
    }
}
