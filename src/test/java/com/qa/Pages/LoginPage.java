package com.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class LoginPage {
    public WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Xpath
    @FindBy(how = How.XPATH, using = "//div//a[@href='/sg/register']")
    private WebElement btnRegister;

    public void clickonRegister() throws Exception {
        if (btnRegister.isDisplayed()) {
            btnRegister.click();
        } else Thread.sleep(5000);
    }

    //Xpath
    @FindBy(how = How.XPATH, using = "//div//input[@name='full_name']")
    private WebElement txtFullName;
    @FindBy(how = How.XPATH, using = "//div//input[@name='email']")
    private WebElement txtEmail;
    @FindBy(how = How.XPATH, using = "(//div//i[@role='presentation'])[1]")
    private WebElement btnCountry;
    @FindBy(how = How.XPATH, using = "(//div//div[@class='q-item__label'])[4]")
    private WebElement txtCountryVN;
    @FindBy(how = How.XPATH, using = "//div//input[@name='phone']")
    private WebElement txtPhone;
    @FindBy(how = How.XPATH, using = "(//div[@class='q-radio__inner relative-position q-radio__inner--falsy'])[1]")
    private WebElement btnRole;
    @FindBy(how = How.XPATH, using = "(//div//i[@role='presentation'])[2]")
    private WebElement btnSelectAboutUs;
    @FindBy(how = How.XPATH, using = "//div//input[@placeholder='Enter the referral code or promo code']")
    private WebElement txtCodeOTP;
    @FindBy(how = How.XPATH, using = "//div[@class='q-checkbox__bg absolute']")
    private WebElement btnCheckBoxConfirm;

    public void fillonInfor() throws Exception {
        txtFullName.clear();
        txtFullName.sendKeys("Tran Duc Duy");
        Thread.sleep(2000);

        Random r = new Random();
        int numbermail = r.nextInt(999);
        txtEmail.clear();
        txtEmail.sendKeys("ducduy" + numbermail + "@yopmail.com");

        btnCountry.click();
        Thread.sleep(2000);
        System.out.println(txtCountryVN.getText().trim());
        System.out.println(txtCountryVN);
        txtCountryVN.click();

        Random r2 = new Random();  // Random number phone
        int numberphone = r2.nextInt(89) + 10;
        int numberphone2 = r2.nextInt(89) + 10;
        txtPhone.clear();
        txtPhone.sendKeys("091" + numberphone + "811" + numberphone2);

        btnRole.click();
        btnSelectAboutUs.click();
        Thread.sleep(2000);

        // Random about us
        List<WebElement> us = driver.findElements(By.xpath("(//div//div[@class='q-item__label'])"));
        int count = us.size();
        System.out.println("Number of links are:" + count);
        Random r4 = new Random();
        int linkNo2 = r4.nextInt(count);
        WebElement us2 = us.get(linkNo2);
        System.out.println(us2.getText());
        Thread.sleep(2000);
        us2.click();

        txtCodeOTP.clear();
        txtCodeOTP.sendKeys("1234");

        btnCheckBoxConfirm.click();
    }

    public void ClosePage() {
        driver.quit();
    }

}

