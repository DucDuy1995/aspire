package com.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

public class IdentityDetails {
    public WebDriver driver;

    public IdentityDetails(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Xpath
    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement btnSubmit;
    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement btnBeginVerify;

    public void fillonIdentityDetails() throws Exception {
        if (btnSubmit.isDisplayed()) {
            // Random about Identity
            List<WebElement> Identity = driver.findElements(By.xpath("//div[@class='auth-form__card']//div[@role='checkbox']"));
            int count = Identity.size();
            System.out.println("Number of links are:" + count);
            Random r = new Random();
            int linkNoo = r.nextInt(count);
            WebElement Identity2 = Identity.get(linkNoo);
            System.out.println(Identity2.getText());
            Thread.sleep(2000);
            Identity2.click();
        } else Thread.sleep(20000);

        btnBeginVerify.click();
        Thread.sleep(2000);
    }
}
