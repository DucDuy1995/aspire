package com.qa.Test.Aspiretest;


import com.qa.Pages.*;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class AspireTestRegister {
    static ExtentTest test;
    static ExtentReports report;

    @BeforeClass
    public static void startTest() {
        report = new ExtentReports(System.getProperty("user.dir") + "\\ReportResults.html");
        test = report.startTest("ExtentAspire");
    }

    public static String capture(WebDriver driver, String filename) throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
        Date date = new Date();
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File Dest = new File("src/../ReportIMG/" + filename + "-" + dateFormat.format(date) + ".png");
        String errflpath = Dest.getAbsolutePath();
        FileUtils.copyFile(scrFile, Dest);
        return errflpath;
    }

    @Test
    public void AspireTest() throws Exception {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        String URL = "https://feature-qa.customer-frontend.staging.aspireapp.com/sg/";
        driver.navigate().to(URL); // Open Page
        Thread.sleep(7000);

        LoginPage loginPage = new LoginPage(driver); // Open Page Register
        loginPage.clickonRegister();
        test.log(LogStatus.PASS, test.addScreenCapture(capture(driver, "ClickButtonRegister")), "Click Register Success");

        loginPage.fillonInfor(); // Fill On Infor Register
        test.log(LogStatus.PASS, test.addScreenCapture(capture(driver, "FillOnInfor")), "Fill On Success");

        OTPMail otpMail = new OTPMail(driver); // Fill On OTP Default 1234
        otpMail.sendOTPMail();
        test.log(LogStatus.PASS, test.addScreenCapture(capture(driver, "PageRegisterCompleted")), "OTP Success");

        BusinessPage businessPage = new BusinessPage(driver);
        businessPage.fillonBusiness();

        // Switch to window
        String ParentWindown = driver.getWindowHandle();
        for (String SubWindown : driver.getWindowHandles()) {
            driver.switchTo().window(SubWindown);
            Thread.sleep(2000);
        }
        businessPage.loginBusiness();
        test.log(LogStatus.ERROR, test.addScreenCapture(capture(driver, "Error400")), "Login Error");

        driver.switchTo().window(ParentWindown); // Back Window Present

        BusinessStandard businessStandard = new BusinessStandard(driver); // Fill On Page Personal Details
        businessStandard.fillonBusinessStandard();
        test.log(LogStatus.PASS, test.addScreenCapture(capture(driver, "PersonalDetails")), "Personal Details");

        BusinessDetails businessDetails = new BusinessDetails(driver); // Fill On Page Business Details
        businessDetails.fillonBusinessDetail();
        test.log(LogStatus.PASS,test.addScreenCapture(capture(driver,"BusinessDetails")),"Business Details");

        IdentityDetails identityDetails = new IdentityDetails(driver); // Fill On Page Identity Details
        identityDetails.fillonIdentityDetails();
        test.log(LogStatus.PASS,test.addScreenCapture(capture(driver,"IdentityDetails")),"Identity Details");

        loginPage.ClosePage(); // Close Page

    }

    @AfterClass
    public static void endTest() {
        report.endTest(test);
        report.flush();
    }
}



